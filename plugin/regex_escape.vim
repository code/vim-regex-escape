"
" regex_escape.vim: Operator to escape regular expression metacharacters to
" make them literal, appropriate to a configurable flavor of regular
" expression, so that:
"   foo * ^bar $\ baz \ quux
" becomes:
"   foo \* \^bar \$\\ baz \\ quux
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_regex_escape') || &compatible || v:version < 700
  finish
endif
let loaded_regex_escape = 1

" Set up mapping
nnoremap <expr> <Plug>(RegexEscape)
      \ regex_escape#()
xnoremap <expr> <Plug>(RegexEscape)
      \ regex_escape#()
