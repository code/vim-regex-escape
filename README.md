regex\_escape.vim
=================

This plugin provides normal and visual mode mapping targets to insert escaping
backslash characters before regular expression metacharacters for text selected
by a motion, as a quick way to put a literal string into a regular expression
context.

It's useful generally as a shortcut, but the author particularly likes it for
keeping track of backslash-heavy expressions where counting gets tiresome.

For example, in the default BRE mode, this string:

   foo * ^bar $\ baz \ quux

Becomes:

   foo \* \^bar \$\\ baz \\ quux

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
